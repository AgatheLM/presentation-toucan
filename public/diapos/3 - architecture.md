# Architecture

---

### Introduction

- réflexion sur l'architecture avec le besoin de lancer l'application en externe pour un prestataire
- réalisation : rendre l'application **non adhérente** à l'environnement Insee

---

### Etape 1 : Etre non-adhérent à l'environnement

![Schéma de l'architecture](diapos/images/archi1.png "Schéma de l'architecture")

---

### Etape 2 : Avoir une application autonome

![Schéma de l'architecture](diapos/images/archi2.png "Schéma de l'architecture")

---

 ### Retour d'expérience

- fonctionnement adopté depuis mars 2020
- utilisation de Gitlab externe pour développer sur PC hors réseau Insee
- très pratique pour développer et très apprécié par toute l'équipe
	- côté démarrage en local avec jeux de données tout prêt, indépendance au niveau de la base de données quand on travaille à plusieurs
	- pc perso / mode hors ligne
