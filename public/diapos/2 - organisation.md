# Organisation

![backlogs](diapos/images/img_backlogs.png "backlogs")

---

## Phase préalable

### Objectifs

- Emergence des besoins
  - Etude de l'existant
- Proposer une solution
  - Test logiciel externe (JEDOX)
  - POC sur l'enjeux de l'agrégation

---

## Phase préalable

### Outils

- Pratiques agiles :
  - cadrer et prioriser
- Bonus :
  - Implication du RIA de Pélican 2
  - Implication forte du CPS et du PO

---

## L'équipe projet

### Dev

- Profils très complémentaires
- Binomage / trinomage

---

## L'équipe projet

### Métier

- CPS
- PO(s)
- Administrateur d'application
- Responsable diffusion

---

## Les échanges avec le métier

- Daily
- US "Atelier"
- Thématiques importantes
  - Agrégation
  - Diffusion via Mélodie

---

## Implication des utilisateurs

- Interviews en phase EP
- Groupe utilisateur, plutot orienté refonte métier (**Au début**)
- Raily test game
- Les démos
- Mise en prod anticipée
- Ateliers R du lundi (**Aujourd'hui**)

---

## Les exploitants

- RIAP présente au quotidien la première année de la phase de réalisation
- Tests de charge en expérimentation devops

---

## Agilité appliquée

- MVP et priorisation
- Planification/priorisation à long terme
- Itérations
- Daily
- [Backlog](https://gitlab.com/InseeFr/national-accounts-domain/toucan/documentation/-/boards/1642020) => élément central
- Demo

---

## Agilité adaptée

- Pas de démo au début
- Estimation des US
- Abandon de suivi graphique de la vélocité ou de l'avancement (burtdown chart...)

---

![](diapos/images/Dende.jpg "Agilité")
