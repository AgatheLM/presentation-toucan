# Production et maintenance

---

## Passage en production

=> Septembre 2020

---

- Exploitation
  - Chaine de déploiement en place
  - Déploiements successifs environ 1 fois par mois (itération)

---

- Utilisation
  - Prise en main de l'application
  - Travaux de migrations des données et des procédures
  - Retours en direct des utilisateurs

---

## Passage en maintenance

=> Septembre 2021

---

- Acteurs en commun
- RIA Pélican 2 = dev toucan = RIA Toucan = Paul
- AA Pélican 2 = AA Toucan

---

- Configuration adaptée
- Backlog initialisé
- Environements éprouvés

---

## Calendrier

- **Septembre 2021** Première version des chaines de traitements Toucan
- **Octobre 2021 – mai 2022** Production de la dernière campagne de comptes sous Pélican
- **Septembre 2021 – Mai 2022** Répetition générale sous Toucan
- **Eté 2022** Validation des chaînes de traitements sous Toucan

---

## Calendrier

- **Décembre 2022** Dernière publication des données produites dans Pélican 2
- **Septembre 2022** Début de la première campagne de comptes utilisant Toucan
- **Mai 2023** Première publication des données produites dans Toucan
